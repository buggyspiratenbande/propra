package main.arrays_06.test.main;

import main.arrays_06.aufgabe2.GameBoard;

import org.junit.Assert;
import org.junit.Test;

public class Aufgabe2Test {


	@Test
	public void testEasyGame() throws Exception {
		GameBoard b = new GameBoard(new int[][] {
				new int[] { 1, 0, 2, 3 },
				new int[] { 4, 5, 6, 7 },
				new int[] { 8, 9, 10, 11 },
				new int[] { 12, 13, 14, 15 }
		});
		b.startGame();
		Assert.assertFalse(b.isSolved());
		b.play(5);
		Assert.assertFalse(b.isSolved());
		b.play(5);
		b.play(1);
		Assert.assertFalse(b.isSolved());
	}
	
	@Test
	public void testEasyGameWithModifiedVictoryCondition() throws Exception {
		GameBoard b = new GameBoard(new int[][] {
				new int[] { 1, 2, 3, 4 },
				new int[] { 5, 6, 7, 8 },
				new int[] { 9, 10, 11, 12 },
				new int[] { 13, 14, 0, 15 }
		});
		b.startGame();
		Assert.assertFalse(b.isSolved());
		b.play(15);
		Assert.assertTrue(b.isSolved());
	}
}
