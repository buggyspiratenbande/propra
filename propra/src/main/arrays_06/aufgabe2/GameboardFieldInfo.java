package main.arrays_06.aufgabe2;

public class GameboardFieldInfo {
	private int number;
	private int x;
	private  int y;

	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}

	public GameboardFieldInfo(int x, int y, int number){
		this.x = x;
		this.y = y;
		this.number = number;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
}
