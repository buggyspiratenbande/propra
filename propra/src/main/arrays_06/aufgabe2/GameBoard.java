package main.arrays_06.aufgabe2;

public class GameBoard {
	private final int boardHeigth;
	private final int boardWidth;
	private GameboardFieldInfo zeroCoords;
	private GameboardFieldInfo[] validCoords;
	/**
	 * [Zeile][Spalte]
	 */
	private final int[][] board;


	/**
	 * @param board
	 * 	Ein Spielbrett mit vorgegebenen Werten,
	 * 	muss quadratisch sein,
	 * muss alle Werte von 0 bis L�nge*L�nge-1 genau einmal enthalten (dies wird nicht �berpr�ft)
	 */
	public GameBoard(int[][] board) {
		this.board = board;
		boardHeigth = board.length;
		if(boardHeigth <= 0){
			throw new IllegalArgumentException();
		}

		boardWidth = board[0].length;
		if(boardWidth <= 0)
			throw new IllegalArgumentException();

		for(int i=0; i<boardHeigth; i++){
			if(board[i].length != boardWidth) {
				throw new IllegalArgumentException();
			}
		}
		if(boardWidth != boardHeigth){
			throw new IllegalArgumentException();
		}
	}



	public void startGame(){
		zeroCoords = getCoordsOf(0);
		initValidCoordinations();
		renderComplete();
	}

	/**
	 * 
	 * @param choice
	 * @return
	 */
	public boolean play(int choice){
		GameboardFieldInfo validCoord = getValidCoordinations(choice);
		if(validCoord != null){
			board[validCoord.getY()][validCoord.getX()] = zeroCoords.getNumber();
			board[zeroCoords.getY()][zeroCoords.getX()] = validCoord.getNumber();
			zeroCoords.setX(validCoord.getX());
			zeroCoords.setY(validCoord.getY());
			initValidCoordinations();
			renderComplete();
		} else {
			renderInvalidInput();
		}
		return isSolved();
	}

	/**
	 * Alle Zahlen von 1-X angeordnet
	 * Das leere Feld (die 0) muss unten rechts stehen
	 */
	public boolean isSolved(){
		int counter = 1;
		for(int i=0; i<boardHeigth; i++){
			for(int j=0; j<boardWidth; j++){
				if(board[i][j] != counter++){
//					return false;
					if(i == boardHeigth-1 && j == boardWidth-1){
						return board[i][j] == 0; // Wenn man am Ende ist und die 0 raus kommt
					} else {
						return false; // Wenn zwischendurch eine Zahl nicht stimmt
					}
				} 
			}
		}
		return true;
	}

	//--------------------------------------------------------------------------------------------
	//----------------------------------Utility---------------------------------------------------

	private GameboardFieldInfo getValidCoordinations(int number){
		for(int i=0; i<validCoords.length; i++){
			if(validCoords[i].getNumber() == number){
				return validCoords[i];
			}
		}
		return null;
	}

	private void initValidCoordinations() {
		int zeroRow = zeroCoords.getY();
		int zeroColumn = zeroCoords.getX();
		GameboardFieldInfo[] tempValidCoords = new GameboardFieldInfo[4];
		int validCoordCount = 0;

		int northRow = zeroRow - 1;
		int northColumn = zeroColumn;
		if(northRow >= 0 && northRow < boardHeigth){
			tempValidCoords[0] = new GameboardFieldInfo(northColumn, northRow, board[northRow][northColumn]);
			validCoordCount++;
		}

		int southRow = zeroRow + 1;
		int southColumn = zeroColumn;
		if(southRow >= 0 && southRow < boardHeigth){
			tempValidCoords[1] = new GameboardFieldInfo(southColumn, southRow, board[southRow][southColumn]);
			validCoordCount++;
		}

		int westRow = zeroRow;
		int westColumn = zeroColumn - 1;
		if(westColumn >= 0 && westColumn < boardWidth){
			tempValidCoords[2] = new GameboardFieldInfo(westColumn, westRow, board[westRow][westColumn]);
			validCoordCount++;
		}

		int eastRow = zeroRow;
		int eastColumn = zeroColumn + 1;
		if(eastColumn >= 0 && eastColumn < boardWidth){
			tempValidCoords[3] = new GameboardFieldInfo(eastColumn, eastRow, board[eastRow][eastColumn]);
			validCoordCount++;
		}

		validCoords = new GameboardFieldInfo[validCoordCount];
		int validCoordsArrayCounter = 0;
		for(int i=0; i<tempValidCoords.length; i++){
			if(tempValidCoords[i] != null){
				validCoords[validCoordsArrayCounter++] = tempValidCoords[i];
			}
		}
	}

	private GameboardFieldInfo getCoordsOf(int number){
		for(int i=0; i<boardHeigth; i++){
			for(int j=0; j<boardWidth; j++){
				if(board[i][j] == number){
					return new GameboardFieldInfo(j, i, 0);
				}
			}
		}
		throw new IllegalArgumentException("number not in board");
	}

	//--------------------------------------------------------------------------------------------
	//-----------------------------------Alles zum Rendern-----------------------------------------

	private void renderComplete(){
		renderGameBoard();
		renderChoices();
	}

	private void renderInvalidInput(){
		System.out.println("Ung�ltige Eingabe");
	}

	private void renderChoices(){
		System.out.print("W�hle zwischen ");
		for(int i=0; i<validCoords.length; i++){
			System.out.print(formatTrailingZeros(validCoords[i].getNumber()));
			if(i<validCoords.length-1){
				System.out.print(", ");
			}
		}
		System.out.println(": ");
	}

	private void renderGameBoard() {
		for (int i = 0; i < boardHeigth; i++) {
			renderLineSeparator();
			renderGameLine(i);
		}
		renderLineSeparator();
	}

	private void renderLineSeparator() {
		for (int i = 0; i < boardWidth; i++) {
			System.out.print("+--");
		}
		System.out.println("+");
	}

	private void renderGameLine(int line) {
		int[] boardLine = board[line];
		for (int i = 0; i < boardWidth; i++) {
			System.out.print("|" + formatTrailingZeros(boardLine[i]));
		}
		System.out.println("|");
	}

	private String formatTrailingZeros(int number) {
		if (number == 0)
			return "  ";
		return String.format("%02d", number);
	}


}
