package main.arrays_06.aufgabe2;

import main.common.IO;

public class UserInputedGame {
	private static GameBoard board;
	public static void main(String[] args) {
		board = new GameBoard(new int[][] {
				new int[] { 11, 12, 13, 14 },
				new int[] { 1, 2, 3, 4 },
				new int[] { 8, 7, 6, 5 },
				new int[] { 0, 9, 10, 15 }
		});
		//		board= new GameBoard(new int[][]{
		//				new int[]{1,0,2},
		//				new int[]{3,4,5},
		//				new int[]{6,7,8}
		//		});
		play();
	}

	private static void play(){
		board.startGame();
		while(!board.isSolved()){
			int choice = IO.readInt();
			board.play(choice);
		}
		System.out.println("Yippie, gel�st! :)");
	}
}
