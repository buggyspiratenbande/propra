package main.arrays_06.aufgabe1;

import main.common.IO;

public class Aufgabe1 {
	public void run() {

		//Eingabe
		int anzSaenger = IO.readInt("Anzahl Saenger (>0) :");
		int[] anrufeProSaenger = new int[anzSaenger];
		int anrufeTotal = 0;
		for(int i=0; i<anzSaenger; i++) {
			anrufeProSaenger[i] = IO.readInt("Anrufe fuer Saenger " + (i+1) + " (>=0) :");
			anrufeTotal += anrufeProSaenger[i];
		}

		//Ausgabe
		for(int i=0; i<anzSaenger; i++) {
			int prozentZahl = Math.round(anrufeProSaenger[i] / (float) anrufeTotal * 100); //Nicht perfekt.. man erreicht im ganzen keien 100% usw...
			String sternchen = buildSternchen(prozentZahl);
			System.out.println("Saenger " + (i+1) + " " + sternchen + " Anrufe %: " + prozentZahl + "; Anrufe absolut: " + anrufeProSaenger[i]);
		}
	}

	private String buildSternchen(int prozentZahl) {
		StringBuilder sternchen = new StringBuilder(prozentZahl);
		for(int sternchenCounter=0; sternchenCounter<prozentZahl; sternchenCounter++){
			sternchen.append('*');
		}
		return sternchen.toString();
	}
}
